FROM openjdk:8-jre-slim

ENV TZ=UTC
ARG CSV_PATH=./sensor.csv

ARG SENSOR_ID
ENV SENSOR_ID=$SENSOR_ID
ARG AMQP_URL
ENV AMQP_URL=$AMQP_URL
ARG QUEUE_NAME
ENV QUEUE_NAME=$QUEUE_NAME
ARG SENSOR_DELAY
ENV SENSOR_DELAY=$SENSOR_DELAY


RUN mkdir /app

COPY $CSV_PATH /app/sensor.csv
COPY ./build/distributions/*.tar /app/sensor.tar

WORKDIR /app

RUN tar -xf sensor.tar

RUN mv sensor-*/* .

ENTRYPOINT ["bin/sensor", "-i", "/app/sensor.csv"]
