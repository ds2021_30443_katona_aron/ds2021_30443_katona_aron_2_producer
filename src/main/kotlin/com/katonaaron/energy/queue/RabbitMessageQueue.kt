package com.katonaaron.energy.queue

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory

class RabbitMessageQueue(
    connectionString: String,
    private val queueName: String,
) : MessageQueue {
    private val connection: Connection
    private val channel: Channel

    private val messageProperties = AMQP.BasicProperties.Builder()
        .contentType("application/json")
        .headers(
            mapOf(
                "__TypeId__" to "SensorMessage"
            )
        )
        .contentEncoding("UTF-8")
        .deliveryMode(2) // make message persistent
        .build()

    init {
        val factory = ConnectionFactory()
        factory.setUri(connectionString)
        connection = factory.newConnection()
        channel = connection.createChannel()
        channel.queueDeclare(queueName, true, false, false, null)

    }

    override fun sendMessage(message: String) {
        channel.basicPublish("", queueName, messageProperties, message.toByteArray())
        println(" [x] Sent '$message'")
    }

    override fun close() {
        channel.close()
        connection.close()
    }
}
