package com.katonaaron.energy.queue

interface MessageQueue : AutoCloseable {
    fun sendMessage(message: String)
}
