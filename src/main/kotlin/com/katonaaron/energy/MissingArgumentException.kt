package com.katonaaron.energy

class MissingArgumentException(argumentName: String) : RuntimeException("Missing argument: $argumentName.")
