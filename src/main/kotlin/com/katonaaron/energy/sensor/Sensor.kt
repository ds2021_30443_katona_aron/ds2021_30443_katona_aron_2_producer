package com.katonaaron.energy.sensor

interface Sensor {
    fun subscribe(callback: (Double) -> Unit)

    suspend fun start()
}
