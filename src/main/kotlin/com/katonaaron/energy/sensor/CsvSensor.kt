package com.katonaaron.energy.sensor

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class CsvSensor(
    private val csvPath: String,
    private val delayMillis: Long
) : Sensor {
    private val callbacks = mutableListOf<(Double) -> Unit>()

    override fun subscribe(callback: (Double) -> Unit) {
        callbacks.add(callback)
    }


    override suspend fun start() {
        csvReader().openAsync(csvPath) {
            readAllAsSequence().asFlow()
                .map { it.firstOrNull() }
                .filterNotNull()
                .collect {
                    callbacks.forEach { callback ->
                        callback(it.toDouble())
                    }
                    delay(delayMillis)
                }
        }
    }
}
