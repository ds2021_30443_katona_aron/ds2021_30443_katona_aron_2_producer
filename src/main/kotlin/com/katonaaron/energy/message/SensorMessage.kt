package com.katonaaron.energy.message

import kotlinx.serialization.Serializable

@Serializable
data class SensorMessage(
    val timestamp: Long,
    val sensor_id: String,
    val measurement_value: Double
)
